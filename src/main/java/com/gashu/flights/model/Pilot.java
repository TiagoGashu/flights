/**
 * 
 */
package com.gashu.flights.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author tiago.gashu
 */
@Entity
@Table(name = "PILOT")
@SequenceGenerator(name = "pilot_seq", sequenceName = "pilot_seq", initialValue = 1,
    allocationSize = 50)
public class Pilot {

  private Long id;
  private String name;

  public Pilot() {
    super();
  }

  /**
   * @param id
   * @param name
   */
  public Pilot(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  // GETTERS / SETTERS

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pilot_seq")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
