/**
 * 
 */
package com.gashu.flights.model;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author tiago.gashu
 */
@Entity
@Table(name = "FLIGHT")
@SequenceGenerator(name = "flight_seq", sequenceName = "flight_seq", initialValue = 1,
    allocationSize = 50)
public class Flight {

  private Long id;
  private String flightNumber;
  private FlightStatus flightStatus;

  private City departureCity;
  private LocalDateTime departureDate;
  private City arrivalCity;
  private LocalDateTime arrivalDate;

  private Plane plane;
  private Pilot pilot;

  public Flight() {
    super();
  }

  public Flight(Long id, String flightNumber, FlightStatus flightStatus, City depatureCity,
      LocalDateTime departureDate, City arrivalCity, LocalDateTime arrivalDate, Plane plane,
      Pilot pilot) {
    this.id = id;
    this.flightNumber = flightNumber;
    this.flightStatus = flightStatus;
    this.departureCity = depatureCity;
    this.departureDate = departureDate;
    this.arrivalCity = arrivalCity;
    this.arrivalDate = arrivalDate;
    this.plane = plane;
    this.pilot = pilot;
  }

  // GETTERS / SETTERS

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "flight_seq")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFlightNumber() {
    return flightNumber;
  }

  public void setFlightNumber(String flightNumber) {
    this.flightNumber = flightNumber;
  }

  @Enumerated(value = EnumType.STRING)
  public FlightStatus getFlightStatus() {
    return flightStatus;
  }

  public void setFlightStatus(FlightStatus flightStatus) {
    this.flightStatus = flightStatus;
  }

  @OneToOne
  @JoinColumn(name = "DEPARTURE_CITY_ID")
  public City getDepartureCity() {
    return departureCity;
  }

  public void setDepartureCity(City departureCity) {
    this.departureCity = departureCity;
  }

  public LocalDateTime getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(LocalDateTime departureDate) {
    this.departureDate = departureDate;
  }

  @OneToOne
  @JoinColumn(name = "ARRIVAL_CITY_ID")
  public City getArrivalCity() {
    return arrivalCity;
  }

  public void setArrivalCity(City arrivalCity) {
    this.arrivalCity = arrivalCity;
  }

  public LocalDateTime getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(LocalDateTime arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PLANE_ID")
  public Plane getPlane() {
    return plane;
  }

  public void setPlane(Plane plane) {
    this.plane = plane;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PILOT_ID")
  public Pilot getPilot() {
    return pilot;
  }

  public void setPilot(Pilot pilot) {
    this.pilot = pilot;
  }

}
