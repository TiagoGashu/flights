/**
 * 
 */
package com.gashu.flights.model.domain;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import com.gashu.flights.model.Flight;
import com.gashu.flights.model.FlightStatus;

/**
 * @author tiago.gashu
 *
 */
public class FlightStatusSpecification implements Specification<Flight> {

  private FlightStatus flightStatus;

  public FlightStatusSpecification(FlightStatus status) {
    this.flightStatus = status;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.data.jpa.domain.Specification#toPredicate(javax.persistence.criteria.Root,
   * javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder)
   */
  @Override
  public Predicate toPredicate(Root<Flight> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
    return cb.equal(root.<FlightStatus>get("flightStatus"), this.flightStatus);
  }



}
