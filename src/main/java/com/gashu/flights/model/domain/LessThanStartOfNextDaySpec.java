/**
 * 
 */
package com.gashu.flights.model.domain;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import com.gashu.flights.model.Flight;

/**
 * @author tiago.gashu
 */
public class LessThanStartOfNextDaySpec implements Specification<Flight> {

  private String targetDate;
  private LocalDateTime date;

  public LessThanStartOfNextDaySpec(String targetDate, LocalDateTime ldt) {
    this.targetDate = targetDate;
    // intervalo aberto no inicio do prox dia
    ZonedDateTime zonedDTStartOfNextDay =
        ldt.plusDays(1).toLocalDate().atStartOfDay(ZoneId.systemDefault());
    this.date = zonedDTStartOfNextDay.toLocalDateTime();
  }

  /*
   * @see
   * org.springframework.data.jpa.domain.Specification#toPredicate(javax.persistence.criteria.Root,
   * javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder)
   */
  @Override
  public Predicate toPredicate(Root<Flight> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
    return cb.lessThan(root.<LocalDateTime>get(this.targetDate), this.date);
  }


}
