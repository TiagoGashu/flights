/**
 * 
 */
package com.gashu.flights.model.domain;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import com.gashu.flights.model.City;
import com.gashu.flights.model.Flight;

/**
 * @author tiago.gashu
 */
public class CitySpecification implements Specification<Flight> {

  private String targetCity;
  private City city;

  public CitySpecification(String targetCity, City city) {
    this.targetCity = targetCity;
    this.city = city;
  }

  /*
   * @see
   * org.springframework.data.jpa.domain.Specification#toPredicate(javax.persistence.criteria.Root,
   * javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder)
   */
  @Override
  public Predicate toPredicate(Root<Flight> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
    return cb.equal(root.<City>get(this.targetCity), this.city);
  }

}
