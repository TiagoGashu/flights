/**
 * 
 */
package com.gashu.flights.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author tiago.gashu
 */
@Entity
@Table(name = "PLANE")
@SequenceGenerator(name = "plane_seq", sequenceName = "plane_seq", initialValue = 1,
    allocationSize = 50)
public class Plane {

  private Long id;
  private String brand;
  private String model;

  public Plane() {
    super();
  }

  public Plane(Long id, String brand, String model) {
    this.id = id;
    this.brand = brand;
    this.model = model;
  }

  // GETTERS / SETTERS

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plane_seq")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }



}
