/**
 * 
 */
package com.gashu.flights.model;

/**
 * @author tiago.gashu
 *
 */
public enum FlightStatus {
  SCHEDULED, DELAYED, DEPARTED, LANDED, LANDED_DELAYED, CANCELED;
}
