/**
 * 
 */
package com.gashu.flights.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gashu.flights.json.FlightJson;
import com.gashu.flights.model.FlightStatus;
import com.gashu.flights.service.FlightService;

/**
 * @author tiago.gashu
 */
@RestController
@RequestMapping(path = "api/flights")
@CrossOrigin(origins = "http://localhost:4200")
public class FlightController {
  @Autowired
  private FlightService flightService;

  @GetMapping("/{flightId}")
  public ResponseEntity<FlightJson> getFlight(
      @PathVariable(name = "flightId", required = true) Long flightId) {
    FlightJson json = this.flightService.find(flightId);
    return new ResponseEntity<FlightJson>(json, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<List<FlightJson>> getFlights(
      @RequestParam(name = "flightNumber", required = false) String flightNumber,
      @RequestParam(name = "flightStatus", required = false) FlightStatus flightStatus,
      @RequestParam(name = "departureDate", required = false) @DateTimeFormat(
          iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime departureDate,
      @RequestParam(name = "departureCityId", required = false) Long departureCityId,
      @RequestParam(name = "arrivalDate", required = false) @DateTimeFormat(
          iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime arrivalDate,
      @RequestParam(name = "arrivalCityId", required = false) Long arrivalCityId) {
    System.out.println(TimeZone.getDefault());
    List<FlightJson> flights = this.flightService.findFlights(flightNumber, flightStatus,
        departureDate, departureCityId, arrivalDate, arrivalCityId);
    return new ResponseEntity<List<FlightJson>>(flights, HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<FlightJson> addFlight(@RequestBody(required = true) FlightJson json) {
    FlightJson savedJson = this.flightService.addFlight(json);
    return new ResponseEntity<FlightJson>(savedJson, HttpStatus.OK);
  }

  @PutMapping()
  public ResponseEntity<FlightJson> updateFlight(@RequestBody(required = true) FlightJson json) {
    FlightJson savedJson = this.flightService.updateFlight(json);
    return new ResponseEntity<FlightJson>(savedJson, HttpStatus.OK);
  }

}
