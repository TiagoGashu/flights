/**
 * 
 */
package com.gashu.flights.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gashu.flights.json.CityJson;
import com.gashu.flights.service.CityService;

/**
 * @author tiago.gashu
 */
@RestController
@RequestMapping(path = "api/cities")
@CrossOrigin(origins = "http://localhost:4200")
public class CityController {

  @Autowired
  private CityService cityService;

  @GetMapping("/{cityId}")
  public ResponseEntity<CityJson> getCity(
      @PathVariable(name = "cityId", required = true) Long cityId) {
    CityJson json = this.cityService.find(cityId);
    return new ResponseEntity<CityJson>(json, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<List<CityJson>> getCities(
      @RequestParam(name = "name", required = false) String name) {
    List<CityJson> cities = this.cityService.findCities(name);
    return new ResponseEntity<List<CityJson>>(cities, HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<CityJson> addCity(@RequestBody(required = true) CityJson json) {
    CityJson savedJson = this.cityService.addCity(json);
    return new ResponseEntity<CityJson>(savedJson, HttpStatus.OK);
  }

  @PutMapping()
  public ResponseEntity<CityJson> updateCity(@RequestBody(required = true) CityJson json) {
    CityJson savedJson = this.cityService.updateCity(json);
    return new ResponseEntity<CityJson>(savedJson, HttpStatus.OK);
  }

  @DeleteMapping("/{cityId}")
  public ResponseEntity<CityJson> deleteCity(
      @PathVariable(name = "cityId", required = true) Long cityId) {
    this.cityService.deleteCity(cityId);
    return new ResponseEntity<CityJson>(new CityJson(), HttpStatus.OK);
  }

}
