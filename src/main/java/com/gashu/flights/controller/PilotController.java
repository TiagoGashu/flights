/**
 * 
 */
package com.gashu.flights.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gashu.flights.json.PilotJson;
import com.gashu.flights.service.PilotService;

/**
 * @author tiago.gashu
 */
@RestController
@RequestMapping(path = "api/pilots")
@CrossOrigin(origins = "http://localhost:4200")
public class PilotController {

  @Autowired
  private PilotService pilotService;

  @GetMapping("/{pilotId}")
  public ResponseEntity<PilotJson> getPilot(
      @PathVariable(name = "pilotId", required = true) Long pilotId) {
    PilotJson json = this.pilotService.find(pilotId);
    return new ResponseEntity<PilotJson>(json, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<List<PilotJson>> getPilots(
      @RequestParam(name = "name", required = false) String name) {
    List<PilotJson> pilots = this.pilotService.findPilots(name);
    return new ResponseEntity<List<PilotJson>>(pilots, HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<PilotJson> addPilot(@RequestBody(required = true) PilotJson json) {
    PilotJson savedJson = this.pilotService.addPilot(json);
    return new ResponseEntity<PilotJson>(savedJson, HttpStatus.OK);
  }

  @PutMapping()
  public ResponseEntity<PilotJson> updatePilot(@RequestBody(required = true) PilotJson json) {
    PilotJson savedJson = this.pilotService.updatePilot(json);
    return new ResponseEntity<PilotJson>(savedJson, HttpStatus.OK);
  }

  @DeleteMapping("/{pilotId}")
  public ResponseEntity<PilotJson> deletePilot(
      @PathVariable(name = "pilotId", required = true) Long pilotId) {
    this.pilotService.deletePilot(pilotId);
    return new ResponseEntity<PilotJson>(new PilotJson(), HttpStatus.OK);
  }

}
