/**
 * 
 */
package com.gashu.flights.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gashu.flights.json.PlaneJson;
import com.gashu.flights.service.PlaneService;

/**
 * @author tiago.gashu
 */
@RestController
@RequestMapping(path = "api/planes")
@CrossOrigin(origins = "http://localhost:4200")
public class PlaneController {
  @Autowired
  private PlaneService planeService;

  @GetMapping("/{planeId}")
  public ResponseEntity<PlaneJson> getPlane(
      @PathVariable(name = "planeId", required = true) Long planeId) {
    PlaneJson json = this.planeService.find(planeId);
    return new ResponseEntity<PlaneJson>(json, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<List<PlaneJson>> getPlanes(
      @RequestParam(name = "brand", required = false) String brand,
      @RequestParam(name = "model", required = false) String model) {
    List<PlaneJson> planes = this.planeService.findPlanes(model);
    return new ResponseEntity<List<PlaneJson>>(planes, HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<PlaneJson> addPlane(@RequestBody(required = true) PlaneJson json) {
    PlaneJson savedJson = this.planeService.addPlane(json);
    return new ResponseEntity<PlaneJson>(savedJson, HttpStatus.OK);
  }

  @PutMapping()
  public ResponseEntity<PlaneJson> updatePlane(@RequestBody(required = true) PlaneJson json) {
    PlaneJson savedJson = this.planeService.updatePlane(json);
    return new ResponseEntity<PlaneJson>(savedJson, HttpStatus.OK);
  }

  @DeleteMapping("/{planeId}")
  public ResponseEntity<PlaneJson> deletePlane(
      @PathVariable(name = "planeId", required = true) Long planeId) {
    this.planeService.deletePlane(planeId);
    return new ResponseEntity<PlaneJson>(new PlaneJson(), HttpStatus.OK);
  }
}
