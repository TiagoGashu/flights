/**
 * 
 */
package com.gashu.flights.converter;

import org.springframework.stereotype.Component;
import com.gashu.flights.json.PilotJson;
import com.gashu.flights.model.Pilot;

/**
 * @author tiago.gashu
 */
@Component
public class PilotConverter extends BaseConverter<Pilot, PilotJson> {

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToJson(java.lang.Object)
   */
  @Override
  public PilotJson convertToJson(Pilot entity) {
    if (entity == null) {
      return null;
    }
    return new PilotJson(entity.getId(), entity.getName());
  }

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToEntity(java.io.Serializable)
   */
  @Override
  public Pilot convertToEntity(PilotJson json) {
    if (json == null) {
      return null;
    }
    return new Pilot(json.getId(), json.getName());
  }

}
