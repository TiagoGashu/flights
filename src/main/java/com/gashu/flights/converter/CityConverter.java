/**
 * 
 */
package com.gashu.flights.converter;

import org.springframework.stereotype.Component;
import com.gashu.flights.json.CityJson;
import com.gashu.flights.model.City;

/**
 * @author tiago.gashu
 */
@Component
public class CityConverter extends BaseConverter<City, CityJson> {

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToJson(java.lang.Object)
   */
  @Override
  public CityJson convertToJson(City entity) {
    if (entity == null) {
      return null;
    }
    return new CityJson(entity.getId(), entity.getName());
  }

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToEntity(java.io.Serializable)
   */
  @Override
  public City convertToEntity(CityJson json) {
    if (json == null) {
      return null;
    }
    return new City(json.getId(), json.getName());
  }

}
