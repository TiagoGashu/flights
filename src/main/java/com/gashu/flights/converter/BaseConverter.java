/**
 * 
 */
package com.gashu.flights.converter;

import java.io.Serializable;
import java.util.List;
import com.google.common.collect.Lists;

/**
 * @author tiago.gashu
 */
public abstract class BaseConverter<E, J extends Serializable> {

  public abstract J convertToJson(E entity);

  public abstract E convertToEntity(J json);

  public List<J> convertToJsonArray(List<E> entities) {
    List<J> jsonArr = Lists.newArrayList();
    for (E entity : entities) {
      jsonArr.add(this.convertToJson(entity));
    }
    return jsonArr;
  }

  public List<E> convertToEntities(List<J> jsonArr) {
    List<E> entities = Lists.newArrayList();
    for (J json : jsonArr) {
      entities.add(this.convertToEntity(json));
    }
    return entities;
  }

}
