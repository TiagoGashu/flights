/**
 * 
 */
package com.gashu.flights.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.gashu.flights.json.CityJson;
import com.gashu.flights.json.FlightJson;
import com.gashu.flights.json.PilotJson;
import com.gashu.flights.json.PlaneJson;
import com.gashu.flights.model.City;
import com.gashu.flights.model.Flight;
import com.gashu.flights.model.Pilot;
import com.gashu.flights.model.Plane;

/**
 * @author tiago.gashu
 */
@Component
public class FlightConverter extends BaseConverter<Flight, FlightJson> {

  @Autowired
  private CityConverter cityConverter;
  @Autowired
  private PlaneConverter planeConverter;
  @Autowired
  private PilotConverter pilotConverter;

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToJson(java.lang.Object)
   */
  @Override
  public FlightJson convertToJson(Flight entity) {
    if (entity == null) {
      return null;
    }

    CityJson departureCity = this.cityConverter.convertToJson(entity.getDepartureCity());
    CityJson arrivalCity = this.cityConverter.convertToJson(entity.getArrivalCity());
    PlaneJson plane = this.planeConverter.convertToJson(entity.getPlane());
    PilotJson pilot = this.pilotConverter.convertToJson(entity.getPilot());

    return new FlightJson(entity.getId(), entity.getFlightNumber(), entity.getFlightStatus(),
        departureCity, entity.getDepartureDate(), arrivalCity, entity.getArrivalDate(), plane,
        pilot);
  }

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToEntity(java.io.Serializable)
   */
  @Override
  public Flight convertToEntity(FlightJson json) {
    if (json == null) {
      return null;
    }

    City departureCity = this.cityConverter.convertToEntity(json.getDepartureCity());
    City arrivalCity = this.cityConverter.convertToEntity(json.getArrivalCity());
    Plane plane = this.planeConverter.convertToEntity(json.getPlane());
    Pilot pilot = this.pilotConverter.convertToEntity(json.getPilot());

    return new Flight(json.getId(), json.getFlightNumber(), json.getFlightStatus(), departureCity,
        json.getDepartureDate(), arrivalCity, json.getArrivalDate(), plane, pilot);
  }

}
