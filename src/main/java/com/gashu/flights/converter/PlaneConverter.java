/**
 * 
 */
package com.gashu.flights.converter;

import org.springframework.stereotype.Component;
import com.gashu.flights.json.PlaneJson;
import com.gashu.flights.model.Plane;

/**
 * @author tiago.gashu
 */
@Component
public class PlaneConverter extends BaseConverter<Plane, PlaneJson> {

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToJson(java.lang.Object)
   */
  @Override
  public PlaneJson convertToJson(Plane entity) {
    if (entity == null) {
      return null;
    }
    return new PlaneJson(entity.getId(), entity.getBrand(), entity.getModel());
  }

  /*
   * @see com.gashu.flights.converter.BaseConverter#convertToEntity(java.io.Serializable)
   */
  @Override
  public Plane convertToEntity(PlaneJson json) {
    if (json == null) {
      return null;
    }
    return new Plane(json.getId(), json.getBrand(), json.getModel());
  }

}
