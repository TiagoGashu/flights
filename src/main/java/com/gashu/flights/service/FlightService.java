/**
 * 
 */
package com.gashu.flights.service;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.gashu.flights.converter.FlightConverter;
import com.gashu.flights.json.FlightJson;
import com.gashu.flights.model.Flight;
import com.gashu.flights.model.FlightStatus;
import com.gashu.flights.model.domain.CitySpecification;
import com.gashu.flights.model.domain.FlightNumberSpecification;
import com.gashu.flights.model.domain.FlightStatusSpecification;
import com.gashu.flights.model.domain.GreaterThanOrEqualToStartOfDaySpec;
import com.gashu.flights.model.domain.LessThanStartOfNextDaySpec;
import com.gashu.flights.repository.FlightRepository;
import com.google.common.collect.Lists;

/**
 * @author tiago.gashu
 */
@Transactional
@Component
public class FlightService {
  @Autowired
  private FlightRepository repo;
  @Autowired
  private FlightConverter converter;

  @Autowired
  private CityService cityService;

  public FlightJson find(Long id) {
    return this.converter.convertToJson(this.repo.findOne(id));
  }

  public List<FlightJson> findFlights(String flightNumber, FlightStatus flightStatus,
      LocalDateTime departureDate, Long departureCityId, LocalDateTime arrivalDate,
      Long arrivalCityId) {

    List<Specification<Flight>> specs = Lists.newArrayList();
    if (!StringUtils.isEmpty(flightNumber)) {
      specs.add(new FlightNumberSpecification(flightNumber));
    }
    if (flightStatus != null) {
      specs.add(new FlightStatusSpecification(flightStatus));
    }
    if (departureCityId != null) {
      specs.add(
          new CitySpecification("departureCity", this.cityService.findEntity(departureCityId)));
    }
    if (departureDate != null) {
      specs.add(new GreaterThanOrEqualToStartOfDaySpec("departureDate", departureDate));
      specs.add(new LessThanStartOfNextDaySpec("departureDate", departureDate));
    }
    if (arrivalCityId != null) {
      specs.add(new CitySpecification("arrivalCity", this.cityService.findEntity(arrivalCityId)));
    }
    if (arrivalDate != null) {
      specs.add(new GreaterThanOrEqualToStartOfDaySpec("arrivalDate", arrivalDate));
      specs.add(new LessThanStartOfNextDaySpec("arrivalDate", arrivalDate));
    }

    Sort sort = new Sort("departureDate");

    List<Flight> flights = this.repo.findAll(this.buildSpecifications(specs), sort);

    return this.converter.convertToJsonArray(flights);
  }

  public FlightJson addFlight(FlightJson json) {
    if (json.getId() != null) {
      return json;
    }
    Flight flight = this.converter.convertToEntity(json);
    this.repo.save(flight);
    return this.converter.convertToJson(flight);
  }

  public FlightJson updateFlight(FlightJson json) {
    if (json.getId() == null) {
      throw new RuntimeException("The flight " + json.getFlightNumber() + " doesn't exist!");
    }
    Flight flight = this.converter.convertToEntity(json);
    this.repo.save(flight);
    return this.converter.convertToJson(flight);
  }

  // private methods

  private Specification<Flight> buildSpecifications(List<Specification<Flight>> specs) {
    Iterator<Specification<Flight>> it = specs.iterator();
    Specification<Flight> result = null;
    if (it.hasNext()) {
      result = Specifications.where(it.next());
    }
    while (it.hasNext()) {
      result = Specifications.where(result).and(it.next());
    }
    return result;
  }

}
