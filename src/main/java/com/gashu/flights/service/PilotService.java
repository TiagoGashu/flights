/**
 * 
 */
package com.gashu.flights.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.gashu.flights.converter.PilotConverter;
import com.gashu.flights.json.PilotJson;
import com.gashu.flights.model.Pilot;
import com.gashu.flights.repository.PilotRepository;

/**
 * @author tiago.gashu
 */
@Transactional
@Component
public class PilotService {
  @Autowired
  private PilotRepository repo;
  @Autowired
  private PilotConverter converter;

  public PilotJson find(Long id) {
    return this.converter.convertToJson(this.repo.findOne(id));
  }

  public List<PilotJson> findPilots(String name) {
    if (StringUtils.isEmpty(name)) {
      return this.converter.convertToJsonArray(this.repo.findAllOrderByName());
    }

    List<Pilot> pilots = this.repo.findByNameContainingIgnoreCase(name);
    return this.converter.convertToJsonArray(pilots);
  }

  public PilotJson addPilot(PilotJson json) {
    if (json.getId() != null) {
      return json;
    }
    Pilot pilot = this.converter.convertToEntity(json);
    this.repo.save(pilot);
    return this.converter.convertToJson(pilot);
  }

  public PilotJson updatePilot(PilotJson json) {
    if (json.getId() == null) {
      throw new RuntimeException("The pilot " + json.getName() + " doesn't exist!");
    }
    Pilot pilot = this.converter.convertToEntity(json);
    this.repo.save(pilot);
    return this.converter.convertToJson(pilot);
  }

  public void deletePilot(Long pilotId) {
    if (pilotId == null) {
      throw new RuntimeException("The pilot id is required to delete!");
    }
    this.repo.delete(pilotId);
  }
}
