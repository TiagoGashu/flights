/**
 * 
 */
package com.gashu.flights.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.gashu.flights.converter.CityConverter;
import com.gashu.flights.json.CityJson;
import com.gashu.flights.model.City;
import com.gashu.flights.repository.CityRepository;

/**
 * @author tiago.gashu
 */
@Transactional
@Component
public class CityService {

  @Autowired
  private CityRepository repo;
  @Autowired
  private CityConverter converter;

  public City findEntity(Long id) {
    return this.repo.findOne(id);
  }

  public CityJson find(Long id) {
    return this.converter.convertToJson(this.repo.findOne(id));
  }

  public List<CityJson> findCities(String name) {
    if (StringUtils.isEmpty(name)) {
      return this.converter.convertToJsonArray(this.repo.findAllOrderByName());
    }

    List<City> cities = this.repo.findByNameContainingIgnoreCase(name);
    return this.converter.convertToJsonArray(cities);
  }

  public CityJson addCity(CityJson json) {
    if (json.getId() != null) {
      return json;
    }
    City city = this.converter.convertToEntity(json);
    this.repo.save(city);
    return this.converter.convertToJson(city);
  }

  public CityJson updateCity(CityJson json) {
    if (json.getId() == null) {
      throw new RuntimeException("The city " + json.getName() + " doesn't exist!");
    }
    City city = this.converter.convertToEntity(json);
    this.repo.save(city);
    return this.converter.convertToJson(city);
  }

  public void deleteCity(Long cityId) {
    if (cityId == null) {
      throw new RuntimeException("The city id is required to delete!");
    }
    this.repo.delete(cityId);
  }

}
