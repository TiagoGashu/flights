/**
 * 
 */
package com.gashu.flights.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.gashu.flights.converter.PlaneConverter;
import com.gashu.flights.json.PlaneJson;
import com.gashu.flights.model.Plane;
import com.gashu.flights.repository.PlaneRepository;

/**
 * @author tiago.gashu
 */
@Transactional
@Component
public class PlaneService {
  @Autowired
  private PlaneRepository repo;
  @Autowired
  private PlaneConverter converter;

  public PlaneJson find(Long id) {
    return this.converter.convertToJson(this.repo.findOne(id));
  }

  public List<PlaneJson> findPlanes(String model) {
    if (StringUtils.isEmpty(model)) {
      return this.converter.convertToJsonArray(this.repo.findAllOrderByBrandAndModel());
    }

    List<Plane> planes = this.repo.findByModelContainingIgnoreCase(model);
    return this.converter.convertToJsonArray(planes);
  }

  public PlaneJson addPlane(PlaneJson json) {
    if (json.getId() != null) {
      return json;
    }
    Plane plane = this.converter.convertToEntity(json);
    this.repo.save(plane);
    return this.converter.convertToJson(plane);
  }

  public PlaneJson updatePlane(PlaneJson json) {
    if (json.getId() == null) {
      throw new RuntimeException("The plane " + json.getModel() + " doesn't exist!");
    }
    Plane plane = this.converter.convertToEntity(json);
    this.repo.save(plane);
    return this.converter.convertToJson(plane);
  }

  public void deletePlane(Long planeId) {
    if (planeId == null) {
      throw new RuntimeException("The plane id is required to delete!");
    }
    this.repo.delete(planeId);
  }
}
