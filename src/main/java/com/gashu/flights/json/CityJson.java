/**
 * 
 */
package com.gashu.flights.json;

import java.io.Serializable;

/**
 * @author tiago.gashu
 */
public class CityJson implements Serializable {

  /** */
  private static final long serialVersionUID = 3525924717219994341L;

  private Long id;
  private String name;

  public CityJson() {
    super();
  }

  public CityJson(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  // GETTERS / SETTERS

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
