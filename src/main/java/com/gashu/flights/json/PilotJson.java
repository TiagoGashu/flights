/**
 * 
 */
package com.gashu.flights.json;

import java.io.Serializable;

/**
 * @author tiago.gashu
 */
public class PilotJson implements Serializable {

  /** */
  private static final long serialVersionUID = 6703852668493803820L;

  private Long id;
  private String name;

  public PilotJson() {
    super();
  }

  public PilotJson(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  // GETTERS / SETTERS

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
