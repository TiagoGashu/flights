/**
 * 
 */
package com.gashu.flights.json;

import java.io.Serializable;

/**
 * @author tiago.gashu
 */
public class PlaneJson implements Serializable {

  /** */
  private static final long serialVersionUID = 3623683292626740506L;

  private Long id;
  private String brand;
  private String model;

  public PlaneJson() {
    super();
  }

  public PlaneJson(Long id, String brand, String model) {
    this.id = id;
    this.brand = brand;
    this.model = model;
  }

  // GETTERS / SETTERS

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

}
