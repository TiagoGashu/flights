/**
 * 
 */
package com.gashu.flights.json;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.gashu.flights.model.FlightStatus;

/**
 * @author tiago.gashu
 */
public class FlightJson implements Serializable {

  /** */
  private static final long serialVersionUID = -629444667611614042L;

  private Long id;
  private String flightNumber;
  private FlightStatus flightStatus;

  private CityJson departureCity;
  private LocalDateTime departureDate;
  private CityJson arrivalCity;
  private LocalDateTime arrivalDate;

  private PlaneJson plane;
  private PilotJson pilot;

  public FlightJson() {
    super();
  }

  public FlightJson(Long id, String flightNumber, FlightStatus flightStatus, CityJson departureCity,
      LocalDateTime departureDate, CityJson arrivalCity, LocalDateTime arrivalDate, PlaneJson plane,
      PilotJson pilot) {
    this.id = id;
    this.flightNumber = flightNumber;
    this.flightStatus = flightStatus;
    this.departureCity = departureCity;
    this.departureDate = departureDate;
    this.arrivalCity = arrivalCity;
    this.arrivalDate = arrivalDate;
    this.plane = plane;
    this.pilot = pilot;
  }

  // GETTERS / SETTERS

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFlightNumber() {
    return flightNumber;
  }

  public void setFlightNumber(String flightNumber) {
    this.flightNumber = flightNumber;
  }

  public FlightStatus getFlightStatus() {
    return flightStatus;
  }

  public void setFlightStatus(FlightStatus flightStatus) {
    this.flightStatus = flightStatus;
  }

  public CityJson getDepartureCity() {
    return departureCity;
  }

  public void setDepartureCity(CityJson departureCity) {
    this.departureCity = departureCity;
  }

  public LocalDateTime getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(LocalDateTime departureDate) {
    this.departureDate = departureDate;
  }

  public CityJson getArrivalCity() {
    return arrivalCity;
  }

  public void setArrivalCity(CityJson arrivalCity) {
    this.arrivalCity = arrivalCity;
  }

  public LocalDateTime getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(LocalDateTime arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public PlaneJson getPlane() {
    return plane;
  }

  public void setPlane(PlaneJson plane) {
    this.plane = plane;
  }

  public PilotJson getPilot() {
    return pilot;
  }

  public void setPilot(PilotJson pilot) {
    this.pilot = pilot;
  }

}
