package com.gashu.flights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.gashu.flights.*"})
public class FlightsApplication {

  public static void main(String[] args) {
    SpringApplication.run(FlightsApplication.class, args);
  }
}
