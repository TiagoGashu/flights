/**
 * 
 */
package com.gashu.flights.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gashu.flights.model.Plane;

/**
 * @author tiago.gashu
 */
public interface PlaneRepository extends CrudRepository<Plane, Long> {

  List<Plane> findByModelContainingIgnoreCase(String model);

  @Query("FROM Plane p ORDER BY p.brand, p.model")
  List<Plane> findAllOrderByBrandAndModel();

}
