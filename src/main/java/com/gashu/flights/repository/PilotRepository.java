/**
 * 
 */
package com.gashu.flights.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gashu.flights.model.Pilot;

/**
 * @author tiago.gashu
 */
public interface PilotRepository extends CrudRepository<Pilot, Long> {

  List<Pilot> findByNameContainingIgnoreCase(String name);

  @Query("FROM Pilot p ORDER BY p.name")
  List<Pilot> findAllOrderByName();
}
