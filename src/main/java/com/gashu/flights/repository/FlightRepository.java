/**
 * 
 */
package com.gashu.flights.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.gashu.flights.model.Flight;

/**
 * @author tiago.gashu
 */
public interface FlightRepository
    extends JpaRepository<Flight, Long>, JpaSpecificationExecutor<Flight> {

}
