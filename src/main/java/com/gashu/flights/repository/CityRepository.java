/**
 * 
 */
package com.gashu.flights.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gashu.flights.model.City;

/**
 * @author tiago.gashu
 */
public interface CityRepository extends CrudRepository<City, Long> {

  List<City> findByNameContainingIgnoreCase(String name);

  @Query("FROM City c ORDER BY c.name")
  List<City> findAllOrderByName();
}
