/**
 * 
 */
package com.gashu.flights.model.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.gashu.flights.converter.CityConverter;
import com.gashu.flights.converter.FlightConverter;
import com.gashu.flights.converter.PilotConverter;
import com.gashu.flights.converter.PlaneConverter;
import com.gashu.flights.json.CityJson;
import com.gashu.flights.json.FlightJson;
import com.gashu.flights.json.PilotJson;
import com.gashu.flights.json.PlaneJson;
import com.gashu.flights.model.City;
import com.gashu.flights.model.Flight;
import com.gashu.flights.model.FlightStatus;
import com.gashu.flights.model.Pilot;
import com.gashu.flights.model.Plane;
import com.gashu.flights.service.CityService;
import com.gashu.flights.service.FlightService;
import com.gashu.flights.service.PilotService;
import com.gashu.flights.service.PlaneService;

/**
 * @author tiago.gashu
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class FlightSearchQueryTest {

  @Autowired
  private EntityManager em;
  @Autowired
  private CityService cityService;
  @Autowired
  private FlightService flightService;
  @Autowired
  private PlaneService planeService;
  @Autowired
  private PilotService pilotService;

  @Autowired
  private CityConverter cityConverter;
  @Autowired
  private PlaneConverter planeConverter;
  @Autowired
  private PilotConverter pilotConverter;
  @Autowired
  private FlightConverter flightConverter;

  private CityJson guarulhos;
  private CityJson beloHorizonte;

  @Before
  public void setup() {
    // persistir cenario inicial
    CityJson guarulhos = this.cityConverter.convertToJson(new City(null, "Guarulhos"));
    CityJson beloHorizonte = this.cityConverter.convertToJson(new City(null, "Belo Horizonte"));
    CityJson portoAlegre = this.cityConverter.convertToJson(new City(null, "Porto Alegre"));

    PlaneJson a320 = this.planeConverter.convertToJson(new Plane(null, "Airbus", "A320"));
    PlaneJson p757 = this.planeConverter.convertToJson(new Plane(null, "Boeing", "757"));

    PilotJson jose = this.pilotConverter.convertToJson(new Pilot(null, "José da Silva"));
    PilotJson joao = this.pilotConverter.convertToJson(new Pilot(null, "João Oliveira"));

    guarulhos = this.cityService.addCity(guarulhos);
    beloHorizonte = this.cityService.addCity(beloHorizonte);
    portoAlegre = this.cityService.addCity(portoAlegre);

    a320 = this.planeService.addPlane(a320);
    p757 = this.planeService.addPlane(p757);

    jose = this.pilotService.addPilot(jose);
    joao = this.pilotService.addPilot(joao);

    this.em.flush();

    this.guarulhos = guarulhos;
    this.beloHorizonte = beloHorizonte;

    LocalDateTime departureDateTime1 =
        LocalDateTime.of(LocalDate.of(2018, 3, 10), LocalTime.of(12, 30));

    LocalDateTime arrivalDateTime1 =
        LocalDateTime.of(LocalDate.of(2018, 3, 10), LocalTime.of(14, 00));

    FlightJson f1Json = this.flightConverter.convertToJson(new Flight(null, "QWE0001",
        FlightStatus.LANDED, this.cityConverter.convertToEntity(guarulhos), departureDateTime1,
        this.cityConverter.convertToEntity(beloHorizonte), arrivalDateTime1,
        this.planeConverter.convertToEntity(a320), this.pilotConverter.convertToEntity(jose)));

    LocalDateTime departureDateTime2 =
        LocalDateTime.of(LocalDate.of(2018, 3, 11), LocalTime.of(23, 30));

    LocalDateTime arrivalDateTime2 =
        LocalDateTime.of(LocalDate.of(2018, 3, 12), LocalTime.of(2, 30));

    FlightJson f2Json = this.flightConverter.convertToJson(new Flight(null, "ZXC0010",
        FlightStatus.SCHEDULED, this.cityConverter.convertToEntity(guarulhos), departureDateTime2,
        this.cityConverter.convertToEntity(portoAlegre), arrivalDateTime2,
        this.planeConverter.convertToEntity(a320), this.pilotConverter.convertToEntity(joao)));

    this.flightService.addFlight(f1Json);
    this.flightService.addFlight(f2Json);

    this.em.flush();

  }

  @Test
  public void flightSearchTest() {
    // busca por flight number
    List<FlightJson> flights1 =
        this.flightService.findFlights("QWE0001", null, null, null, null, null);
    Assert.assertTrue("deveria vir 1 voo", flights1 != null && flights1.size() == 1);

    // busca por status
    List<FlightJson> flights2 =
        this.flightService.findFlights(null, FlightStatus.LANDED, null, null, null, null);
    Assert.assertTrue("deveria vir 1 voo", flights2 != null && flights2.size() == 1);

    // busca por flight number e status
    List<FlightJson> flights3 =
        this.flightService.findFlights("QWE0001", FlightStatus.LANDED, null, null, null, null);
    Assert.assertTrue("deveria vir 1 voo", flights3 != null && flights3.size() == 1);

    // busca por depatureDate
    LocalDateTime ldt1 = LocalDateTime.of(LocalDate.of(2018, 3, 10), LocalTime.of(12, 30));
    List<FlightJson> flights4 = this.flightService.findFlights(null, null, ldt1, null, null, null);
    Assert.assertTrue("deveria vir 1 voo", flights4 != null && flights4.size() == 1);

    // busca por departureDate e departureCity
    List<FlightJson> flights5 =
        this.flightService.findFlights(null, null, ldt1, this.guarulhos.getId(), null, null);
    Assert.assertTrue("deveria vir 1 voo", flights5 != null && flights5.size() == 1);

    // buscar por depatureDate e departureCity => nao deveria vir ngm
    List<FlightJson> flights6 =
        this.flightService.findFlights(null, null, ldt1, this.beloHorizonte.getId(), null, null);
    Assert.assertTrue("nao deveria encontrar nenhum voo", flights6 == null || flights6.size() == 0);
  }

}
